import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ScrollToModule } from 'ng2-scroll-to';
import { ModalModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ImagesPresentComponent } from './images-present/images-present.component';
import { AboutComponent } from './about/about.component';
import { MyMenuComponent } from './my-menu/my-menu.component';
import { HomeComponent } from './home/home.component';
import { ChallangeComponent } from './challange/challange.component';
import { NavbarComponent } from './navbar/navbar.component';
import { JoinComponent } from './join/join.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    ImagesPresentComponent,
    AboutComponent,
    MyMenuComponent,
    HomeComponent,
    ChallangeComponent,
    NavbarComponent,
    JoinComponent
  ],
  imports: [
    BrowserModule,
    ScrollToModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
