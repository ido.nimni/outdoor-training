import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-challange',
  templateUrl: './challange.component.html',
  styleUrls: ['./challange.component.css']
})
export class ChallangeComponent implements OnInit {

  @ViewChild('videoPlayer') videoplayer: any;
  on: boolean;
  constructor() {
    this.on = false;
  }

  toggleVideo() {
    if (this.on) {
      this.videoplayer.nativeElement.pause();      
    } else {
      this.videoplayer.nativeElement.play();      
    }
    this.on = !this.on;    
  }

  ngOnInit() {
  }

}
