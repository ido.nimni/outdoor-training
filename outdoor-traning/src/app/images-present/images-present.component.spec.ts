import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagesPresentComponent } from './images-present.component';

describe('ImagesPresentComponent', () => {
  let component: ImagesPresentComponent;
  let fixture: ComponentFixture<ImagesPresentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagesPresentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagesPresentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
