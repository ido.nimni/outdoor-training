import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-images-present',
  templateUrl: './images-present.component.html',
  styleUrls: ['./images-present.component.css']
})
export class ImagesPresentComponent implements OnInit {

  @Input() inputFolder: string;
  images: string[];
  current: number;
  constructor() { }

  prevPic() {
    if (this.current - 1 < 0) {
      this.current = this.images.length - 1;
    } else {
      this.current--;
    }
  }

  nextPic() {
    if (this.current + 1 >= this.images.length) {
      this.current = 0;
    } else {
      this.current++;
    }
  }

  ngOnInit() {
    this.current = 0;
    this.images = [];
    this.images.push("../../assets/ksenia-img.jpg");
    this.images.push("../../assets/title-img.jpg");
    this.images.push("../../assets/model2.jpg");
    this.images.push("../../assets/model.jpg");    
  }

}
